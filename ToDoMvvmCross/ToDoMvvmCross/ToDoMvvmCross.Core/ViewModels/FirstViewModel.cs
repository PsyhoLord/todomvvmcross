using MvvmCross.Core.ViewModels;

namespace ToDoMvvmCross.Core.ViewModels
{
    public class FirstViewModel
        : MvxViewModel
    {
        private string _hello = "Hello MvvmCross";
        public string Hello
        {
            get => _hello;
            set => SetProperty(ref _hello, value);
        }
    }
}
